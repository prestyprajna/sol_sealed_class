﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestA;

namespace Sol_Sealed_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            //sealed class is used if i want to restrict developer, to further inherit from the last or sealed class....

            //Mangal mangalObj = new Mangal();
            //mangalObj.TestMethod();

            Pooja poojaObj = new Pooja();
            poojaObj.TestMethod();
        }
    }
}

namespace TestA
{
    public class Presty
    {
        internal virtual void TestMethod()
        {
            Console.WriteLine("presty method");
        }
    }

    public class Mangal :Presty
    {
        internal override void TestMethod()
        {
            Console.WriteLine("mangal method");
        }
    }

    public sealed class Pooja : Mangal
    {
        internal override void TestMethod()
        {
            Console.WriteLine("pooja method");
        }
    }

    //public class Ravina : Pooja  //no access bcoz pooja claz is sealed
    //{

    //}

}

